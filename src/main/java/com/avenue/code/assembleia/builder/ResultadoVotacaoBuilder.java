package com.avenue.code.assembleia.builder;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.avenue.code.assembleia.dto.CategoriaVotoDto;
import com.avenue.code.assembleia.dto.ResultadoVotacaoDto;
import com.avenue.code.assembleia.enumeration.VotoEnum;
import com.avenue.code.assembleia.model.Pauta;
import com.avenue.code.assembleia.model.Votacao;

/**
 * @author Cesar Leonardo Soares Paim
 *
 */
public class ResultadoVotacaoBuilder {
	
	Logger log = LoggerFactory.getLogger(ResultadoVotacaoBuilder.class);

	private List<Votacao> votacoes;
	private Pauta pauta;

	public static ResultadoVotacaoBuilder builder() {
		return new ResultadoVotacaoBuilder();
	}

	public ResultadoVotacaoBuilder votacoes(List<Votacao> votacoes) {
		this.votacoes = votacoes;
		return this;
	}

	public ResultadoVotacaoBuilder pauta(Pauta pauta) {
		this.pauta = pauta;
		return this;
	}

	public ResultadoVotacaoDto build() {

		ResultadoVotacaoDto resultadoVotacao = new ResultadoVotacaoDto();

		if (Objects.nonNull(pauta)) {
			resultadoVotacao.setNomePauta(pauta.getNome());
			resultadoVotacao.setStatus(pauta.getStatusSessao().name());
			resultadoVotacao.setTotalVotos(0);
		}

		if (!CollectionUtils.isEmpty(votacoes)) {
			resultadoVotacao.setTotalVotos(votacoes.size());
			Arrays.asList(VotoEnum.values()).forEach(ve -> {
				resultadoVotacao
						.add(new CategoriaVotoDto(ve, votacoes.stream().filter(v -> ve == v.getVoto()).count()));
			});
			
		}
		log.info("Resultado votacao {}", resultadoVotacao.toString());
		return resultadoVotacao;
	}
}