package com.avenue.code.assembleia.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.avenue.code.assembleia.dto.VotacaoDto;
import com.avenue.code.assembleia.enumeration.VotoEnum;

@Entity
@Table(name = "VOTACAO", uniqueConstraints = @UniqueConstraint(columnNames = { "PAUTA_ID", "ASSOCIADO_ID" }))
public class Votacao {

	@Id
    @SequenceGenerator(name = "VOTACAO_SEQ", sequenceName = "VOTACAO_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VOTACAO_SEQ")
	private Long id;

	@ManyToOne
	@JoinColumn(referencedColumnName = "ID", name = "PAUTA_ID")
	private Pauta pauta;

	@ManyToOne
	@JoinColumn(referencedColumnName = "ID", name = "ASSOCIADO_ID")
	private Associado associado;

	@Enumerated(EnumType.STRING)
	private VotoEnum voto;

	public Votacao() {
		super();
	}

	public Votacao(VotoEnum voto) {
		this.voto = voto;
	}

	public Votacao(VotacaoDto dto) {
		if (Objects.nonNull(dto)) {
			this.id = dto.getId();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pauta getPauta() {
		return pauta;
	}

	public Associado getAssociado() {
		return associado;
	}

	public void setAssociado(Associado associado) {
		this.associado = associado;
	}

	public VotoEnum getVoto() {
		return voto;
	}

	public void setVoto(VotoEnum voto) {
		this.voto = voto;
	}

	public void setPauta(Pauta pauta) {
		this.pauta = pauta;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}