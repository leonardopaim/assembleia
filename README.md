# README #


### Propósito ###

* Assembleia é um sistema dirigido ao gerenciamento de pautas e votações. 
* Versão 1.0.0

### Configurações ###

* Sistema desenvolvido sobre o framework spring boot 2.3.1.RELEASE  
* Banco de dados utilizado h2database - utilizado em arquivo local
* Teste unitários utilizando Junit e Mockito
* Contexto da aplicação - '/assembleias'
* Porta definida 8085
* Camada de persistência JPA, já embutida no spring boot

### Carga no banco de dados ###
* Dentro do diretório do app há um arquvo (data.sql) que contém inserts para a tabela ASSOCIADO caso não queira utilizar o serviço.

### Console de acesso ao Banco de dados ###
* http://{host}:8085/assembleias/h2-console
* user: sa
* password: não preecnher


### Serviços ###

* Em uma visão macro, a aplicação se divide em três contextos:
	* /assembleias/pautas
	* /assembleias/votacoes
	* /assembleias/associados
	
	
* /assembleias/pautas
	* POST
		* Exemplo: 
			{
				"nome" : "Aqui nome da Pauta",
				"descricao" : "Descricao da pauta",
				"tempo_sessao_aberta" : 5
			}
	* PUT
		* URL /assembleias/pautas/{id} (Atualizar uma pauta)
		* Exemplo: {"id" : 10, "nome" : "Aqui Pauta para atualizar", "descricao" : "Descricao da pauta atualizada", "tempo_sessao_aberta" : 4}
	* GET
		* URL: /assembleias/pautas/all (Listar todas as pautas)
		* URL: /assembleias/pautas/{id} (Carregar uma pauta pelo id)
		* URL: /assembleias/pautas/abrir-votacoes/{id}/{tempo} (Iniciar a votação de uma pauta pelo {id} com o {tempo} em minutos )
		* URL: /assembleias/pautas/abrir-votacoes/{id} (Iniciar a votação de uma pauta pelo {id})

* /assembleias/votacoes
	* POST
		* Exemplo: {"id_associado" : 3, "id_pauta" : 7, "voto" : "sim" } (operação que insere o voto dos associados)
	* GET
		* URL: /assembleias/votacoes/resultados/{id} (Buscar o resultado de uma votação)

* /assembleias/associados
	* POST
		* Exemplo: { "nome" : "Cesar Paim", "documento" : "51064884008" } 
	* GET
		* URL: /assembleias/associados (Consulta todos os asosciados cadstrados na base)
		* URL: /assembleias/associados/{id} (Caonsulta um associado pelo {id})
