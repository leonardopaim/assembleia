package com.avenue.code.assembleia.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.avenue.code.assembleia.exception.AssembleiaException;

@Component
public class UserInfoUrlConfig {
	
	@Value("${user.info.url:}")
	private String url;
	
	public String buildUrl(String documento) {
		if (StringUtils.isEmpty(url)) {
			throw new AssembleiaException("URL não encontrada para o paramêtro user.info.url ");
		}
		return url.concat(documento);
	}
}