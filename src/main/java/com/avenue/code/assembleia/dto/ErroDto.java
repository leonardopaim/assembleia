package com.avenue.code.assembleia.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class ErroDto {
	
	private String campo;
	private String mensagem;
		
	public ErroDto(String campo, String erro) {
		super();
		this.campo = campo;
		this.mensagem = erro;
	}
	
	public ErroDto(String msg) {
		super();
		this.mensagem = msg;
	}

	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String msg) {
		this.mensagem = msg;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

}