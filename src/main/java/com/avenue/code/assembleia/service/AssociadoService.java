package com.avenue.code.assembleia.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.avenue.code.assembleia.model.Associado;
import com.avenue.code.assembleia.repository.AssociadoRepository;

/**
 * @author Cesar Leonardo Soares Paim
 *
 */
@Service
public class AssociadoService {

	private final AssociadoRepository repository;

	public AssociadoService(AssociadoRepository repository) {
		this.repository = repository;
	}

	/**
	 * @return {@link List<Associado>}
	 */
	public List<Associado> consultarTodos() {
		return repository.findAll();
	}
	
	/**
	 * @param {@link Long} id
	 * @return {@link Associado}
	 */
	public Associado consultar(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	/**
	 * @param {@link Long} id
	 * @return {@link Associado}
	 */
	public Associado salvar(Associado associado) {
		return repository.save(associado);
	}


}