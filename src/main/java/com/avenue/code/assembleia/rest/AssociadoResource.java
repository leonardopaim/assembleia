package com.avenue.code.assembleia.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avenue.code.assembleia.dto.AssociadoDto;
import com.avenue.code.assembleia.model.Associado;
import com.avenue.code.assembleia.service.AssociadoService;

/**
 * @author César Leonardo Soares Paim
 *
 */
@RestController
@RequestMapping("/associados")
public class AssociadoResource {
	
	private final AssociadoService service;
	
	public AssociadoResource(AssociadoService service) {
		this.service = service;
	}

	/**
	 * @return List<Associado>
	 */
	@GetMapping
	public ResponseEntity<List<Associado>> consultarTodos() {
		return ResponseEntity.ok(service.consultarTodos());
	}
	
	/**
	 * @param id
	 * @return AssociadoDto
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Associado> consultar(@PathVariable("id") Long id) {
		return ResponseEntity.ok(service.consultar(id));
	}
	
	/**
	 * @param {@linkplain AssociadoDto}
	 * @return ResponseEntity<AssociadoDto>
	 */
	@PostMapping
	public ResponseEntity<AssociadoDto> save(@RequestBody @Valid AssociadoDto associado) {
		return ResponseEntity.ok(new AssociadoDto(service.salvar(new Associado(associado))));
	}
}