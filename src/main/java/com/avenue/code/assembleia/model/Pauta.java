package com.avenue.code.assembleia.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.avenue.code.assembleia.dto.PautaDto;
import com.avenue.code.assembleia.enumeration.StatusSessao;

@Entity
public class Pauta {
	
	@Id
    @SequenceGenerator(name = "PAUTA_SEQ", sequenceName = "PAUTA_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAUTA_SEQ")
	private Long id;
	
	private String nome;
	
	private String descricao;
	
	@NotNull
	private Long tempoSessaoAberta;
	
	private LocalDateTime dataAtivacao;
	
	private Boolean notificacaoSessaoFinalizada;
	
    @OneToMany(mappedBy = "pauta")
	private List<Votacao> votacoes;
	
	public Pauta() { }
	
	public Pauta(Long id, String nome, String descricao) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public Pauta(PautaDto pauta) {
		if (Objects.nonNull(pauta)) {
			this.descricao = pauta.getDescricao();
			this.id = pauta.getId();
			this.nome = pauta.getNome();
			this.tempoSessaoAberta = pauta.getTempoSessaoAberta();
			this.dataAtivacao = pauta.getDataAtivacao();
		}
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getTempoSessaoAberta() {
		return tempoSessaoAberta;
	}

	public void setTempoSessaoAberta(Long tempoSessaoAberta) {
		this.tempoSessaoAberta = tempoSessaoAberta;
	}

	public LocalDateTime getDataAtivacao() {
		return dataAtivacao;
	}

	public void setDataAtivacao(LocalDateTime dataAtivacao) {
		this.dataAtivacao = dataAtivacao;
	}
	
	public List<Votacao> getVotacoes() {
		return votacoes;
	}
	
	public void setVotacoes(List<Votacao> votacoes) {
		this.votacoes = votacoes;
	}
	
	public void setNotificacaoSessaoFinalizada(Boolean notificacaoSessaoFinalizada) {
		this.notificacaoSessaoFinalizada = notificacaoSessaoFinalizada;
	}
	
	public Boolean getNotificacaoSessaoFinalizada() {
		return notificacaoSessaoFinalizada;
	}
	
	public StatusSessao getStatusSessao() {
		if (Objects.isNull(getDataAtivacao())) {
			return StatusSessao.FECHADA;
		}
		if (getDataAtivacao().plusMinutes(tempoSessaoAberta).compareTo(LocalDateTime.now()) < 0) {
			return StatusSessao.ENCERRADA;
		}
		return StatusSessao.ABERTA;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}


}