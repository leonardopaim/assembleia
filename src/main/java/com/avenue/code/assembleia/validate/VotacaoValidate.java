package com.avenue.code.assembleia.validate;

import java.util.Objects;

import org.springframework.stereotype.Component;

import com.avenue.code.assembleia.dto.VotacaoDto;
import com.avenue.code.assembleia.enumeration.StatusSessao;
import com.avenue.code.assembleia.enumeration.VotoEnum;
import com.avenue.code.assembleia.exception.AssembleiaException;
import com.avenue.code.assembleia.model.Associado;
import com.avenue.code.assembleia.model.Pauta;
import com.avenue.code.assembleia.repository.VotacaoRepository;
import com.avenue.code.assembleia.service.UserInfoService;

/**
 * @author Cesar Leonardo Soares Paim
 *
 */
@Component
public class VotacaoValidate {
	
	private final VotacaoRepository repository;
	private final UserInfoService userInfoService;
	
	public VotacaoValidate(VotacaoRepository repository, UserInfoService userInfoService) {
		this.repository = repository;
		this.userInfoService = userInfoService;
	}

	/**
	 * Metodo responsável por fazer validações para inicializar votação
	 * @param pauta
	 * @param associado
	 * @param dto
	 */
	public void validar(Pauta pauta, Associado associado, VotacaoDto dto) {
		String naoEncontrado = "Não encontrado(a) na base de dados"; 
		if (Objects.isNull(pauta)) {
			throw new AssembleiaException("id_pauta", naoEncontrado);
		}
		if (StatusSessao.FECHADA == pauta.getStatusSessao() || StatusSessao.ENCERRADA == pauta.getStatusSessao()) {
			throw new AssembleiaException("id_pauta", "Não é possível votar nessa pauta.");
		}
		if (Objects.isNull(associado)) {
			throw new AssembleiaException("id_associado", naoEncontrado);
		}
		if (Objects.isNull(VotoEnum.fromString(dto.getVoto()))) {
			throw new AssembleiaException("voto", "Não foi possível identificar o seu voto. Por favor utilize 'S' | 'SIM' ou 'N' | 'NAO'.");
		}
		if (repository.existsByAssociadoIdAndPautaId(associado.getId(), pauta.getId())) {
			throw new AssembleiaException("id_associado, id_pauta", "O associado já votou nessa pauta.");
		}
		if (!userInfoService.isHabilitadoParaVotar(associado.getDocumento())) {
			throw new AssembleiaException("id_associado", "O associado não tem permissão para votar.");
		}
	}
}