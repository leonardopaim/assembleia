package com.avenue.code.assembleia.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avenue.code.assembleia.model.Associado;

public interface AssociadoRepository extends JpaRepository<Associado, Long> {

}