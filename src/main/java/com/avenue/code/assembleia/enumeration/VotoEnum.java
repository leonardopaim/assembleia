package com.avenue.code.assembleia.enumeration;

import java.util.Arrays;

import org.springframework.util.StringUtils;

public enum VotoEnum {
	
	S,N;
	
	public static VotoEnum fromString(String voto) {
		if (!StringUtils.isEmpty(voto)) {
			Boolean sim = Arrays.asList("s", "Sim", "1", "y", "yes")
			.stream()
			.anyMatch(s -> s.equalsIgnoreCase(voto));
			if (sim) {
				return VotoEnum.S;
			}
			Boolean nao = Arrays.asList("n", "Não", "Nao", "0", "no")
			.stream()
			.anyMatch(s -> s.equalsIgnoreCase(voto));
			if (nao) {
				return VotoEnum.N;
			}
		}
		return null;
	}
}
