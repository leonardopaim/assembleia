package com.avenue.code.assembleia.dto;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.util.CollectionUtils;

import com.avenue.code.assembleia.model.Votacao;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class VotacaoDto {
	
	private Long id;
	
	@NotNull
	private Long idPauta;
	
	@NotNull
	private Long idAssociado;
	
	@NotNull
	private String voto; 
	
	public VotacaoDto() {
		
	}
	
	public VotacaoDto(Votacao votacao) {
		if (Objects.nonNull(votacao)) {
			this.id = votacao.getId();
			this.idPauta = Objects.nonNull(votacao.getPauta()) ? votacao.getPauta().getId() : null;
			this.idAssociado = Objects.nonNull(votacao.getAssociado()) ? votacao.getAssociado().getId() : null;
			this.voto = votacao.getVoto().name();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPauta() {
		return idPauta;
	}

	public void setIdPauta(Long idPauta) {
		this.idPauta = idPauta;
	}
	
	public String getVoto() {
		return voto;
	}
	
	public void setVoto(String voto) {
		this.voto = voto;
	}
	
	public Long getIdAssociado() {
		return idAssociado;
	}
	
	
	public void setIdAssociado(Long idAssociado) {
		this.idAssociado = idAssociado;
	}
	
	public static List<VotacaoDto> converter(List<Votacao> list) {
		return CollectionUtils.isEmpty(list) ? null
				: list.stream().map(VotacaoDto::new).collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	

}