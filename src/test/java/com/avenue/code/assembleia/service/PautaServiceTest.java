package com.avenue.code.assembleia.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.avenue.code.assembleia.exception.AssembleiaException;
import com.avenue.code.assembleia.model.Pauta;
import com.avenue.code.assembleia.repository.PautaRepository;

@SpringBootTest
public class PautaServiceTest {

	@Test
	public void testAbrirSessaoPautaNaoIdentificada() {
		try {
			getPautaService().abrirVotacao(30L);
		} catch (AssembleiaException e) {
			assertTrue(e.getMessage().contains("Pauta não identificada"));
		}
	}

	@Test
	public void testAbrirSessaoPautaJaAberta() {
		try {
			getPautaService().abrirVotacao(10L);
		} catch (AssembleiaException e) {
			assertTrue(e.getMessage().contains("Essa pauta já foi aberta"));
		}
	}

	@Test
	public void testAbrirSessaoPautaOk() {
		Long minutosSessaoAberta = 40L;
		Pauta pauta = getPautaService().abrirVotacao(20L, minutosSessaoAberta);
		assertTrue(minutosSessaoAberta.compareTo(pauta.getTempoSessaoAberta()) == 0);
	}

	private PautaRepository getPautaRepository() {
		PautaRepository repository = mock(PautaRepository.class);
		when(repository.findById(10L)).thenReturn(Optional.of(getPauta(10L, null, LocalDateTime.now())));
		when(repository.findById(20L)).thenReturn(Optional.of(getPauta(20L, null, null)));
		when(repository.findById(30L)).thenReturn(Optional.empty());
		return repository;
	}

	private Pauta getPauta(Long id, Long tempoSessaoAberta, LocalDateTime localDateTime) {
		Pauta pauta = new Pauta();
		pauta.setId(id);
		pauta.setDataAtivacao(localDateTime);
		pauta.setTempoSessaoAberta(tempoSessaoAberta);
		return pauta;
	}

	private PautaService getPautaService() {
		return new PautaService(getPautaRepository());
	}
}
