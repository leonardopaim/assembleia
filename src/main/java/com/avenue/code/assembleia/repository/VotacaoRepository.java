package com.avenue.code.assembleia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.avenue.code.assembleia.model.Votacao;

public interface VotacaoRepository extends JpaRepository<Votacao, Long> {
	
	 boolean existsByAssociadoIdAndPautaId(Long associadoId, Long pautaId);
	 
	 List<Votacao> findAllByPautaId(Long pautaId);
	 
}