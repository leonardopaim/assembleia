package com.avenue.code.assembleia.service;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.avenue.code.assembleia.builder.ResultadoVotacaoBuilder;
import com.avenue.code.assembleia.dto.ResultadoVotacaoDto;
import com.avenue.code.assembleia.dto.VotacaoDto;
import com.avenue.code.assembleia.enumeration.VotoEnum;
import com.avenue.code.assembleia.exception.AssembleiaException;
import com.avenue.code.assembleia.model.Associado;
import com.avenue.code.assembleia.model.Pauta;
import com.avenue.code.assembleia.model.Votacao;
import com.avenue.code.assembleia.repository.AssociadoRepository;
import com.avenue.code.assembleia.repository.PautaRepository;
import com.avenue.code.assembleia.repository.VotacaoRepository;
import com.avenue.code.assembleia.validate.VotacaoValidate;

/**
 * @author Cesar Leonardo Soares Paim
 *
 */
@Service
public class VotacaoService {

	Logger log = LoggerFactory.getLogger(VotacaoService.class);

	private final PautaRepository pautaRepository;
	private final VotacaoRepository votacaoRepository;
	private final AssociadoRepository associadoRepository;
	private final VotacaoValidate validador;

	public VotacaoService(PautaRepository pautaRepository, VotacaoRepository sessaoVotacaoRepository,
			AssociadoRepository associadoRepository, VotacaoValidate validate) {
		this.pautaRepository = pautaRepository;
		this.votacaoRepository = sessaoVotacaoRepository;
		this.associadoRepository = associadoRepository;
		this.validador = validate;
	}

	/**
	 * @param dto
	 * @return Votacao
	 */
	public Votacao salvar(VotacaoDto dto) {

		Pauta pauta = pautaRepository.findById(dto.getIdPauta()).orElse(null);
		Associado associado = associadoRepository.findById(dto.getIdAssociado()).orElse(null);

		validador.validar(pauta, associado, dto);

		Votacao votacao = new Votacao(dto);
		votacao.setPauta(pauta);
		votacao.setAssociado(associado);
		votacao.setVoto(VotoEnum.fromString(dto.getVoto()));

		votacaoRepository.save(votacao);
		log.info("Voto do associado {} na pauta {} efetuado com sucesso.", associado.getId(), pauta.getId());
		return votacao;
	}

	/**
	 * @return List<Votacao>
	 */
	public List<Votacao> buscarTodos() {
		return votacaoRepository.findAll();
	}

	/**
	 * @param idPauta
	 * @return ResultadoVotacaoDto
	 */
	public ResultadoVotacaoDto consultarResultadoVotacao(Long idPauta) {
		Pauta pauta = pautaRepository.findOneByIdLeftJoinVotacoes(idPauta);
		if (Objects.isNull(pauta)) {
			throw new AssembleiaException("id", "Pauta não identificada na base de dados.");
		}
		return ResultadoVotacaoBuilder.builder().pauta(pauta)
				.votacoes(Objects.nonNull(pauta) ? pauta.getVotacoes() : null).build();
	}
}