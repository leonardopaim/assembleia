package com.avenue.code.assembleia.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.avenue.code.assembleia.dto.AssociadoDto;

@Entity
public class Associado {

	@Id
    @SequenceGenerator(name = "ASSOCIADO_SEQ", sequenceName = "ASSOCIADO_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASSOCIADO_SEQ")
	private Long id;

	private String nome;

	private String documento;
	
	public Associado() {
	}
	
	public Associado(AssociadoDto dto) {
		if (Objects.nonNull(dto)) {
			this.documento = dto.getDocumento();
			this.id = dto.getId();
			this.nome = dto.getNome();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

}