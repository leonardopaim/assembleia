package com.avenue.code.assembleia.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.avenue.code.assembleia.enumeration.StatusSessao;
import com.avenue.code.assembleia.model.Pauta;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PautaDto {
	
	private Long id;
	
	@NotEmpty
	private String nome;
	
	@NotEmpty
	private String descricao;
	
	private Long tempoSessaoAberta;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private LocalDateTime dataAtivacao;
	
	private StatusSessao status; 
	
	public PautaDto() {
		this.status = StatusSessao.FECHADA;
		this.tempoSessaoAberta = 1L;
	}
	
	public PautaDto(Pauta pauta) {
		if (Objects.nonNull(pauta)) {
			this.id = pauta.getId();
			this.nome = pauta.getNome();
			this.descricao = pauta.getDescricao();
			this.tempoSessaoAberta = pauta.getTempoSessaoAberta();
			this.dataAtivacao = pauta.getDataAtivacao();
			this.status = pauta.getStatusSessao();
		}
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public StatusSessao getStatus() {
		return status;
	}
	
	public void setStatus(StatusSessao status) {
		this.status = status;
	}
	
	public Long getTempoSessaoAberta() {
		return tempoSessaoAberta;
	}

	public void setTempoSessaoAberta(Long tempoSessaoAberta) {
		this.tempoSessaoAberta = tempoSessaoAberta;
	}

	public LocalDateTime getDataAtivacao() {
		return dataAtivacao;
	}

	public void setDataAtivacao(LocalDateTime dataAtivacao) {
		this.dataAtivacao = dataAtivacao;
	}
	
	public static List<PautaDto> converter(List<Pauta> list) {
		return list.stream().map(PautaDto::new).collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}


}