package com.avenue.code.assembleia.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.avenue.code.assembleia.enumeration.VotoEnum;

/**
 * @author Cesar Leonardo Soares Paim
 *
 */
public class CategoriaVotoDto {
	
	private VotoEnum voto;
	private Long quantidade;
	
	public CategoriaVotoDto() {
	
	}
	
	public CategoriaVotoDto(VotoEnum voto, Long quantidade) {
		this.voto = voto;
		this.quantidade = quantidade;
	}

	public VotoEnum getVoto() {
		return voto;
	}
	public void setVoto(VotoEnum voto) {
		this.voto = voto;
	}
	public Long getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}


}