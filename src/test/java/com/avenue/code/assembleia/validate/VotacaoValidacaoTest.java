package com.avenue.code.assembleia.validate;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.avenue.code.assembleia.dto.VotacaoDto;
import com.avenue.code.assembleia.exception.AssembleiaException;
import com.avenue.code.assembleia.model.Associado;
import com.avenue.code.assembleia.model.Pauta;
import com.avenue.code.assembleia.repository.VotacaoRepository;
import com.avenue.code.assembleia.service.UserInfoService;
import com.avenue.code.assembleia.util.ValidadorCpf;

@SpringBootTest
class VotacaoValidacaoTest {
	
	@Test
	public void testAssociadoJaVotou() {
		
		VotacaoValidate validate = getVotacaoValidate();
		
		Associado associado = new Associado();
		associado.setId(1L);
		
		Pauta pauta = getPauta(1L);
		
		VotacaoDto votacao = new VotacaoDto();
		votacao.setVoto("S");
		
		try {
			validate.validar(pauta, associado, votacao);
		} catch(AssembleiaException ex) {
			assertTrue(ex.getMessage().contains("O associado já votou nessa pauta"));
		}
	}
	
	@Test
	public void testAssociadoVotouErrado() {
		
		VotacaoValidate validate = getVotacaoValidate();
		
		Associado associado = new Associado();
		associado.setId(1L);
		
		Pauta pauta = getPauta(1L);
		
		VotacaoDto votacao = getVotacaoDto("Yess");
		
		try {
			validate.validar(pauta, associado, votacao);
		} catch(AssembleiaException ex) {
			assertTrue(ex.getMessage().contains("Não foi possível identificar o seu voto"));
		}
	}

	private VotacaoDto getVotacaoDto(String voto) {
		VotacaoDto votacao = new VotacaoDto();
		votacao.setVoto(voto);
		return votacao;
	}
	
	@Test
	public void testAssociadoEnviouVotouNulo() {
		
		VotacaoValidate validate = getVotacaoValidate();
		
		Associado associado = new Associado();
		associado.setId(1L);
		
		Pauta pauta = getPauta(1L);
		
		VotacaoDto votacao = new VotacaoDto();
		votacao.setVoto(null);
		
		try {
			validate.validar(pauta, associado, votacao);
		} catch(AssembleiaException ex) {
			assertTrue(ex.getMessage().contains("Não foi possível identificar o seu voto"));
		}
	}
	
	@Test
	public void testAssociadoNaoEncontrado() {
		
		VotacaoValidate validate = getVotacaoValidate();
		
		Associado associado = new Associado();
		associado.setId(1L);
		
		Pauta pauta = null;
		
		VotacaoDto votacao = new VotacaoDto();
		votacao.setVoto("S");
		
		try {
			validate.validar(pauta, associado, votacao);
		} catch(AssembleiaException ex) {
			assertTrue(ex.getMessage().contains("Não encontrado(a) na base de dados"));
		}
	}
	
	@Test
	public void testPautaNaoEncontrada() {
		
		VotacaoValidate validate = getVotacaoValidate();
		
		Associado associado = null;
		
		Pauta pauta = getPauta(1L);
		
		VotacaoDto votacao = new VotacaoDto();
		votacao.setVoto("S");
		
		try {
			validate.validar(pauta, associado, votacao);
		} catch(AssembleiaException ex) {
			assertTrue(ex.getMessage().contains("Não encontrado(a) na base de dados"));
		}
	}
	
	@Test
	public void testCpfInvalido() {
		
		VotacaoValidate validate = getVotacaoValidate();
		
		Associado associado = new Associado();
		associado.setId(2L);
		associado.setDocumento("81141670090");
		
		Pauta pauta = getPauta(1L);
		
		VotacaoDto votacao = new VotacaoDto();
		votacao.setVoto("S");
		
		try {
			validate.validar(pauta, associado, votacao);
		} catch(AssembleiaException ex) {
			assertTrue(ex.getMessage().contains("O CPF do associado é inválido"));
		}
	}
	
	@Test
	public void testCpfValido() {
		
		VotacaoValidate validate = getVotacaoValidate();
		
		Associado associado = new Associado();
		associado.setId(2L);
		associado.setDocumento("81141670097");
		
		Pauta pauta = getPauta(1L);
		
		VotacaoDto votacao = new VotacaoDto();
		votacao.setVoto("S");
		
		validate.validar(pauta, associado, votacao);
		assertTrue(true); // sem exception, quer dizer que passou por todoas as validacoes
	}

	private Pauta getPauta(Long id) {
		Pauta pauta = new Pauta();
		pauta.setId(id);
		pauta.setDataAtivacao(LocalDateTime.now());
		pauta.setTempoSessaoAberta(10L);
		return pauta;
	}

	private VotacaoRepository getVotacaoRepository() {
		VotacaoRepository repository = mock(VotacaoRepository.class);
		when(repository.existsByAssociadoIdAndPautaId(1L, 1L)).thenReturn(true);
		return repository;
	}
	
	private UserInfoService getUserInfoService() {
		UserInfoService userInfoService = mock(UserInfoService.class);
		when(userInfoService.isHabilitadoParaVotar("81141670097")).thenReturn(ValidadorCpf.isCPF("81141670097"));
		when(userInfoService.isHabilitadoParaVotar("81141670090")).thenThrow(new AssembleiaException("O CPF do associado é inválido"));
		return userInfoService;
	}
	
	private VotacaoValidate getVotacaoValidate() {
		return new VotacaoValidate(getVotacaoRepository(), getUserInfoService());
	}
}