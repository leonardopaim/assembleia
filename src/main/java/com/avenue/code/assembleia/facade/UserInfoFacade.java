package com.avenue.code.assembleia.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.avenue.code.assembleia.config.UserInfoUrlConfig;
import com.avenue.code.assembleia.dto.UserInfoResponseDto;

@Component
public class UserInfoFacade {
	
	Logger log = LoggerFactory.getLogger(UserInfoFacade.class);
	
	
	private final UserInfoUrlConfig userInfoUrlConfig;
    private final RestTemplate restTemplate;


	public UserInfoFacade(UserInfoUrlConfig userInfoUrlConfig, RestTemplate restTemplate) {
		this.userInfoUrlConfig = userInfoUrlConfig;
		this.restTemplate = restTemplate;
	}
	
	public UserInfoResponseDto consultarUserInfo(String numeroDocumento) {
    	String url = userInfoUrlConfig.buildUrl(numeroDocumento);
    	log.info("Request: ".concat(url));
    	ResponseEntity<UserInfoResponseDto> response = restTemplate.exchange(url, HttpMethod.GET, null, UserInfoResponseDto.class);
    	log.info("Response: ".concat(response.getBody().toString()));
		return response.getBody();
    }
}
