package com.avenue.code.assembleia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.avenue.code.assembleia.model.Pauta;

public interface PautaRepository extends JpaRepository<Pauta, Long> {
	
	@Query("select p from Pauta p left join fetch p.votacoes where p.id = :paramId")
	Pauta findOneByIdLeftJoinVotacoes(@Param("paramId") Long id);
	
	@Query("select p from Pauta p where p.dataAtivacao is not null and "
			+ "(p.notificacaoSessaoFinalizada is null or p.notificacaoSessaoFinalizada is false)")
	List<Pauta> findAllToIntegrate();

}
