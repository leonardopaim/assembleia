package com.avenue.code.assembleia.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ResultadoVotacaoDto {
	
	private String nomePauta;
	private Integer totalVotos;
	private List<CategoriaVotoDto> votos;
	private String status;
	
	public String getNomePauta() {
		return nomePauta;
	}
	public void setNomePauta(String nomePauta) {
		this.nomePauta = nomePauta;
	}
	public Integer getTotalVotos() {
		return totalVotos;
	}
	public void setTotalVotos(Integer totalVotos) {
		this.totalVotos = totalVotos;
	}
	public List<CategoriaVotoDto> getVotos() {
		return votos;
	}
	public void setVotos(List<CategoriaVotoDto> votos) {
		this.votos = votos;
	}
	public void add(CategoriaVotoDto voto) {
		if (Objects.isNull(votos)) {
			votos = new ArrayList<>();
		}
		votos.add(voto);
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}


}