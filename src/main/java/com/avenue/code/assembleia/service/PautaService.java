package com.avenue.code.assembleia.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.avenue.code.assembleia.exception.AssembleiaException;
import com.avenue.code.assembleia.model.Pauta;
import com.avenue.code.assembleia.repository.PautaRepository;

/**
 * @author Cesar Leonardo Soares Paim
 *
 */
@Service
public class PautaService {

	Logger log = LoggerFactory.getLogger(PautaService.class);

	private final PautaRepository repository;

	public PautaService(PautaRepository pautaRepository) {
		this.repository = pautaRepository;
	}

	/**
	 * @param id
	 * @return Pauta
	 */
	public Pauta abrirVotacao(Long id) {
		return abrirVotacao(id, null);
	}

	/**
	 * @param id
	 * @param tempoSessaoAberta
	 * @return Pauta
	 */
	public Pauta abrirVotacao(Long id, Long tempoSessaoAberta) {

		Optional<Pauta> entity = repository.findById(id);

		if (entity.isPresent()) {
			Pauta pauta = entity.get();

			if (Objects.nonNull(pauta.getDataAtivacao())) {
				throw new AssembleiaException("Essa pauta já foi aberta");
			}

			if (Objects.nonNull(tempoSessaoAberta)) {
				pauta.setTempoSessaoAberta(tempoSessaoAberta);
			}

			pauta.setDataAtivacao(LocalDateTime.now());
			repository.save(pauta);
			log.info("Pauta de votação {} aberta com sucesso ", pauta.getId());
			return pauta;
		} else {
			throw new AssembleiaException("Pauta não identificada.");
		}
	}

	/**
	 * @return List<Pauta>
	 */
	public List<Pauta> findAll() {
		return repository.findAll();
	}

	/**
	 * Retorna uma {@link Pauta} através de um id {@link Long}.
	 *
	 * @param id must not be {@literal null}.
	 * @return uma {@link Pauta}
	 */
	public Pauta findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	/**
	 * salva uma {@link Pauta}.
	 *
	 * @param {@linkplain Pauta}.
	 * @return uma {@link Pauta}
	 */
	public Pauta save(Pauta pauta) {
		return repository.save(pauta);
	}

	/**
	 * @param id
	 */
	public void delete(Long id) {
		repository.deleteById(id);
	}
}