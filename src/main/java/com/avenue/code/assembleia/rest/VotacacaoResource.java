package com.avenue.code.assembleia.rest;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avenue.code.assembleia.dto.ResultadoVotacaoDto;
import com.avenue.code.assembleia.dto.VotacaoDto;
import com.avenue.code.assembleia.service.VotacaoService;

/**
 * @author César Leonardo Soares Paim
 *
 */
@RestController
@RequestMapping("/votacoes")
public class VotacacaoResource {
	
	private final VotacaoService service;
	
	public VotacacaoResource(VotacaoService service) {
		this.service = service;
	}

	/**
	 * @param votacao
	 * @return ResponseEntity<VotacaoDto>
	 */
	@PostMapping
	public ResponseEntity<VotacaoDto> save(@RequestBody @Valid VotacaoDto votacao) {
		return ResponseEntity.ok(new VotacaoDto(service.salvar(votacao)));
	}
	
	/**
	 * @param idPauta
	 * @return ResponseEntity<ResultadoVotacaoDto>
	 */
	@GetMapping("/resultados/{id}")
	public ResponseEntity<ResultadoVotacaoDto> abrirVotacao(@PathVariable("id") Long idPauta) {
		return ResponseEntity.ok(service.consultarResultadoVotacao(idPauta));
	}
}