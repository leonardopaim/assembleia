package com.avenue.code.assembleia.dto;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.br.CPF;

import com.avenue.code.assembleia.model.Associado;

public class AssociadoDto {
	
	private Long id;
	
	@NotNull
	private String nome;
	
	@NotNull
	@CPF
	private String documento;
	
	public AssociadoDto() {
	}
	
	public AssociadoDto(Associado associado) {
		if (Objects.nonNull(associado)) {
			this.id = associado.getId();
			this.documento = associado.getDocumento();
			this.nome = associado.getNome();
		}
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public static List<AssociadoDto> converter(List<Associado> list) {
		return list.stream().map(AssociadoDto::new).collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}