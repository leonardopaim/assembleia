package com.avenue.code.assembleia.service;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;

import com.avenue.code.assembleia.dto.UserInfoResponseDto;
import com.avenue.code.assembleia.exception.AssembleiaException;
import com.avenue.code.assembleia.facade.UserInfoFacade;

/**
 * @author Cesar Leonardo Soares Paim
 *
 */
@Service
public class UserInfoService {
	
	Logger log = LoggerFactory.getLogger(PautaService.class);
	
	private final UserInfoFacade userInfoFacade;
	
    /**
     *  valor default é {@value}
     */
    private final String habilitadoParaVotar = "ABLE_TO_VOTE";
	
	public UserInfoService(UserInfoFacade facade) {
		this.userInfoFacade = facade;
	}
	
	/**
	 * metodo responsável por fazer avalaição do CPF do associado consultando uma fonte externa
	 * @param numeroDocumento
	 * @return Boolean
	 */
	public Boolean isHabilitadoParaVotar(String numeroDocumento) {
		try {
			UserInfoResponseDto userInfo = userInfoFacade.consultarUserInfo(numeroDocumento);
			if (Objects.nonNull(userInfo) && !StringUtils.isEmpty(userInfo.getStatus())) {
				return habilitadoParaVotar.equalsIgnoreCase(userInfo.getStatus());
			}
			return false;
		} catch (HttpClientErrorException e) {
			if (HttpStatus.NOT_FOUND == e.getStatusCode()) {
				throw new AssembleiaException("O CPF do associado é inválido");
			}
			log.error(e.getMessage(), e);
			throw new AssembleiaException("Erro ao consultar a situação do CPF do associado. Por favor, procure o administrador do sistema.", e);
		}
	}
}