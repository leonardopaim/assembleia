package com.avenue.code.assembleia.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.avenue.code.assembleia.dto.PautaDto;
import com.avenue.code.assembleia.model.Pauta;
import com.avenue.code.assembleia.service.PautaService;

/**
 * @author César Leonardo Soares Paim
 *
 */
@RestController
@RequestMapping("/pautas")
public class PautaResource {
	
	private final PautaService service;
	
	public PautaResource(PautaService service) {
		this.service = service;
	}

	/**
	 * @param id
	 * @return ResponseEntity<PautaDto>
	 */
	@GetMapping("/abrir-votacoes/{id}")
	public ResponseEntity<PautaDto> abrirVotacao(@PathVariable("id") Long id) {
		return ResponseEntity.ok(new PautaDto(service.abrirVotacao(id)));
	}
	
	/**
	 * @param id
	 * @param tempo
	 * @return ResponseEntity<PautaDto>
	 */
	@GetMapping("/abrir-votacoes/{id}/{tempo}")
	public ResponseEntity<PautaDto> abrirVotacaoComTempo(@PathVariable("id") Long id, @PathVariable("tempo") Long tempo) {
		return ResponseEntity.ok(new PautaDto(service.abrirVotacao(id, tempo)));
	}

	/**
	 * @param id
	 * @return ResponseEntity<PautaDto> 
	 */
	@GetMapping("/status-votacoes/{id}")
	public ResponseEntity<PautaDto> consultarStatusVotacao(@PathVariable("id") Long id) {
		return ResponseEntity.ok(new PautaDto(service.abrirVotacao(id)));
	}
	
	/**
	 * @param id
	 * @return ResponseEntity<PautaDto>
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<PautaDto> delete(@RequestParam("id") Long id) {
		service.delete(id);
		return ResponseEntity.ok(null);
	}
	
	/**
	 * @param id
	 * @return ResponseEntity<PautaDto>
	 */
	@GetMapping("/{id}")
	public ResponseEntity<PautaDto> find(@PathVariable("id") Long id) {
		return ResponseEntity.ok(new PautaDto(service.findById(id)));
	}
	
	/**
	 * @return ResponseEntity<List<PautaDto>>
	 */
	@GetMapping("/all")
	public ResponseEntity<List<PautaDto>> findAll() {
		return ResponseEntity.ok(PautaDto.converter(service.findAll()));
	}
	
	/**
	 * @param pauta
	 * @return ResponseEntity<PautaDto>
	 */
	@PostMapping
	public ResponseEntity<PautaDto> save(@RequestBody @Valid PautaDto pauta) {
		return ResponseEntity.ok(new PautaDto(service.save(new Pauta(pauta))));
	}
	
	/**
	 * @param pauta
	 * @return ResponseEntity<PautaDto>
	 */
	@PutMapping("/{id}")
	public ResponseEntity<PautaDto> update(@RequestBody @Valid PautaDto pauta, @PathVariable("id") Long id) {
		return ResponseEntity.ok(new PautaDto(service.save(new Pauta(pauta))));
	}

}