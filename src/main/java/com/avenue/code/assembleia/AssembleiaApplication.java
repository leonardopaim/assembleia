package com.avenue.code.assembleia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

/**
 * @author Cesar Leonardo Soares Paim
 *
 */
@SpringBootApplication
@EnableScheduling
public class AssembleiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssembleiaApplication.class, args);
	}
	
	/**
	 * @return {@linkplain RestTemplate} 
	 */
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

}