package com.avenue.code.assembleia.config;

import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.avenue.code.assembleia.dto.ErroDto;
import com.avenue.code.assembleia.exception.AssembleiaException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

@RestControllerAdvice
public class ErroValidacaoHandler {
	
	Logger log = LoggerFactory.getLogger(ErroValidacaoHandler.class);
	
	@Autowired
	private MessageSource messageSource;
	
	@Value("${spring.jackson.date-format:}")
	private String dateFormat;
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ConstraintViolationException.class)
	public List<ErroDto> handle(ConstraintViolationException exception) {
		List<ErroDto> erroDtos = new ArrayList<>();
		exception.getConstraintViolations().forEach(c -> erroDtos.add(new ErroDto(c.getPropertyPath().toString(), c.getMessage())));
		log.error(erroDtos.toString(), exception);
		return erroDtos;
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(AssembleiaException.class)
	public ErroDto handle(AssembleiaException exception) {
		ErroDto dto = new ErroDto(exception.getCampo(), exception.getMessage());
		log.error(dto.toString(), exception);
		return dto; 
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<ErroDto> handle(MethodArgumentNotValidException exception) {
		List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
		List<ErroDto> erroDtos = new ArrayList<>();
		fieldErrors.forEach(e -> {
			String messagem = messageSource.getMessage(e, LocaleContextHolder.getLocale()); 
			ErroDto erro = new ErroDto(e.getField(), messagem);
			erroDtos.add(erro);
		});
		log.error(erroDtos.toString(), exception);
		return erroDtos;
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public List<ErroDto> handle(HttpMessageNotReadableException ex) {
		if (Objects.nonNull(ex.getCause()) && ex.getCause() instanceof InvalidFormatException
				&& Objects.nonNull(ex.getMostSpecificCause()) && ex.getMostSpecificCause() instanceof DateTimeParseException) {
			InvalidFormatException invalidFormat = (InvalidFormatException) ex.getCause();
			invalidFormat.getPathReference();
			List<ErroDto> erroDtos = invalidFormat.getPath().stream()
				.map(p -> new ErroDto(p.getFieldName(), "O formato de data utilizado na aplicação é ".concat(dateFormat)))
				.collect(Collectors.toList());
			log.error(erroDtos.toString(), ex);
		}
		return Arrays.asList(new ErroDto(ex.getMostSpecificCause().getMessage()));
	}
}