package com.avenue.code.assembleia.scheduling;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.avenue.code.assembleia.enumeration.StatusSessao;
import com.avenue.code.assembleia.model.Pauta;
import com.avenue.code.assembleia.repository.PautaRepository;

@Component
public class AssembleiaScheduled {

	Logger log = LoggerFactory.getLogger(AssembleiaScheduled.class);

	private final PautaRepository repository;

	public AssembleiaScheduled(PautaRepository pautaRepository) {
		this.repository = pautaRepository;
	}

	@Scheduled(fixedRateString = "${fixedRate.in.milliseconds}")
	public void integrarVotacoesFinalizadas() {
		
		List<Pauta> pautas = repository.findAllToIntegrate();
		
		pautas = pautas.stream()
				.filter(p -> StatusSessao.ENCERRADA == p.getStatusSessao())
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(pautas)) {
			return;
		}
		
		log.info("Inicio integracao de pautas");
		
		if (!CollectionUtils.isEmpty(pautas)) {
			log.info("{} pautas para integrar.", pautas.size());
			pautas.stream().filter(p -> StatusSessao.ENCERRADA == p.getStatusSessao()).forEach(p -> {
				try {
					Thread.sleep(2000);
					integrarPauta(p);
					log.info("Pauta {} integrada com sucesso.", p.getId());
					repository.save(p);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});
		}
		log.info("Fim processo integração pautas");
	}

	private Pauta integrarPauta(Pauta pauta) {
		pauta.setNotificacaoSessaoFinalizada(true);
		return pauta;
	}
}
