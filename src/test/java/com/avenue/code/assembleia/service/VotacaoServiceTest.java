package com.avenue.code.assembleia.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.avenue.code.assembleia.dto.CategoriaVotoDto;
import com.avenue.code.assembleia.dto.ResultadoVotacaoDto;
import com.avenue.code.assembleia.enumeration.StatusSessao;
import com.avenue.code.assembleia.enumeration.VotoEnum;
import com.avenue.code.assembleia.model.Pauta;
import com.avenue.code.assembleia.model.Votacao;
import com.avenue.code.assembleia.repository.PautaRepository;

@SpringBootTest
public class VotacaoServiceTest {

	@Test
	public void testResultadoVotacaoNomePauta() {
		ResultadoVotacaoDto resultado = getVotacaoService().consultarResultadoVotacao(10L);
		
		assertTrue(resultado.getNomePauta().equalsIgnoreCase("Pauta de teste"));
	}
	
	@Test
	public void testResultadoVotacaoTotalVotos() {
		ResultadoVotacaoDto resultado = getVotacaoService().consultarResultadoVotacao(10L);
		assertTrue(resultado.getTotalVotos().compareTo(8) == 0);
	}
	
	@Test
	public void testResultadoVotacaoTotalVotosSim() {
		
		ResultadoVotacaoDto resultado = getVotacaoService().consultarResultadoVotacao(10L);
		
		Optional<CategoriaVotoDto> optional = resultado.getVotos().stream()
				.filter(cv -> VotoEnum.S == cv.getVoto())
				.findFirst();
			
			assertTrue(optional.get().getQuantidade().compareTo(5L) == 0);
	}
	
	@Test
	public void testResultadoVotacaoTotalVotosNao() {
		
		ResultadoVotacaoDto resultado = getVotacaoService().consultarResultadoVotacao(10L);
		
		Optional<CategoriaVotoDto> optional = resultado.getVotos().stream()
			.filter(cv -> VotoEnum.N == cv.getVoto())
			.findFirst();
		
		assertTrue(optional.get().getQuantidade().compareTo(3L) == 0);
	}
	
	@Test
	public void testResultadoVotacaoStatusEncerrada() {
		
		ResultadoVotacaoDto resultado = getVotacaoService().consultarResultadoVotacao(10L);
		assertTrue(StatusSessao.ENCERRADA.name().equalsIgnoreCase(resultado.getStatus()));
	}

	private PautaRepository getPautaRepository() {
		PautaRepository repository = mock(PautaRepository.class);
		when(repository.findOneByIdLeftJoinVotacoes(10L)).thenReturn(getPauta(10L));
		when(repository.findOneByIdLeftJoinVotacoes(20L)).thenReturn(null);
		return repository;
	}

	private Pauta getPauta(Long id) {
		Pauta pauta = new Pauta();
		pauta.setId(id);
		pauta.setNome("Pauta de teste");
		pauta.setDataAtivacao(LocalDateTime.now().minusMinutes(20L));
		pauta.setTempoSessaoAberta(1L);
		pauta.setVotacoes(Arrays.asList(new Votacao(VotoEnum.S),new Votacao(VotoEnum.S),new Votacao(VotoEnum.S),
				new Votacao(VotoEnum.N), new Votacao(VotoEnum.S),new Votacao(VotoEnum.N),new Votacao(VotoEnum.N),
				new Votacao(VotoEnum.S)));
		return pauta;
	}

	private VotacaoService getVotacaoService() {
		return new VotacaoService(getPautaRepository(), null, null, null);
	}
}