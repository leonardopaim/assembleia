package com.avenue.code.assembleia.exception;

public class AssembleiaException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7103589872411463057L;
	
	private String campo;

	public AssembleiaException(String campo, String message) {
		super(message);
		this.campo = campo;
	}
	
	public AssembleiaException(String message) {
		super(message);
	}
	
    public AssembleiaException(String message, Throwable cause) {
        super(message, cause);
    }

	public String getCampo() {
		return campo;
	}
	
	public void setCampo(String campo) {
		this.campo = campo;
	}

}
